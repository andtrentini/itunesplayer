var trackPlaying = -1;
var playing = false;

function initSearchElement() {
  var searchElement = document.getElementById('main');
  searchElement.innerHTML =
    `<div class="input-group">
        <input type="text" class="form-control" placeholder="Nome artista da cercare" aria-label="Nome artista da cercare" 
        aria-describedby="button-addon2" id="artist-name-input"  onkeyup="artistSearch()">
        <button class="btn btn-outline-secondary" type="button" id="search-button" onclick="artistSearch()">Cerca</button>
    </div>
    <div class="mt-5" id="results"></div>`;
  var navbar = document.getElementById('menu');
  if (navbar.childNodes.length > 2) {
    navbar.removeChild(navbar.lastChild);
  }
}

function artistSearch() {
  var client = new XMLHttpRequest();
  var artistName = document.getElementById('artist-name-input').value;

  client.onreadystatechange = () => {
    if (client.readyState == 4 && client.status == 200) {
      var artists = JSON.parse(client.response)
      createArtistSearchResult(artists.results);
    }
  }

  client.open('GET', 'https://itunes.apple.com/search?entity=musicArtist&term=' + artistName);
  client.send();
}

function albumSearch(artistId) {
  var client = new XMLHttpRequest();
  var main = document.getElementById('main').value;
  client.onreadystatechange = () => {
    if (client.readyState == 4 && client.status == 200) {
      var albums = JSON.parse(client.response)
      albums = albums.results.splice(1);
      createAlbumGrid(albums);
    }
  }

  client.open('GET', 'https://itunes.apple.com/lookup?limit=200&entity=album&id=' + artistId);
  client.send();
}

function getTrackList(album) {
  var client = new XMLHttpRequest();
  var main = document.getElementById('main').value;

  client.onreadystatechange = () => {
    if (client.readyState == 4 && client.status == 200) {
      var tracks = JSON.parse(client.response)
      tracks = tracks.results;
      var main = document.getElementById('main');
      main.innerHTML = '';
      main.appendChild(createTracksView(tracks));
    }
  }

  client.open('GET', 'https://itunes.apple.com/lookup?limit=200&entity=song&id=' + album.collectionId);
  client.send();
}

function createArtistSearchResult(artists) {
  var artistList = document.createElement('div');
  artistList.className = 'list-group';
  artists.forEach(artist => {
    var artistElement = document.createElement('button');
    artistElement.classList = 'list-group-item list-group-item-action d-flex justify-content-between align-items-center';
    artistElement.innerText = artist.artistName;
    artistElement.onclick = () => {
      albumSearch(artist.artistId);
    }
    if (artist.primaryGenreName) {
      var artistGenre = document.createElement('span');
      artistGenre.classList = 'badge bg-primary rounded-pill';
      artistGenre.innerText = artist.primaryGenreName;
      artistElement.appendChild(artistGenre);
    }
    artistList.appendChild(artistElement);
  });

  var resultsElement = document.getElementById('results');
  resultsElement.innerHTML = '';
  resultsElement.appendChild(artistList);
}

function createAlbumGrid(albums) {
  var albumGrid = document.createElement('div');
  albumGrid.className = 'row';
  albums.forEach(album => {
    albumGrid.appendChild(createAlbumCard(album));
  });
  var main = document.getElementById('main');
  main.innerHTML = '';
  main.appendChild(albumGrid);
}

function createAlbumCard(album) {
  var cardContainer = document.createElement('div');
  cardContainer.classList = 'col-sm-4 mb-3';
  var card = document.createElement('div');
  cardContainer.appendChild(card);
  card.onclick = () => {
    getTrackList(album);
  }
  card.className = 'card';

  var cardImage = document.createElement('img');
  card.appendChild(cardImage);
  cardImage.className = 'card-img-top';
  cardImage.src = album.artworkUrl100;

  var cardBody = document.createElement('div');
  card.appendChild(cardBody);
  cardBody.className = 'card-body';
  var cardTitle = document.createElement('h5');
  cardBody.appendChild(cardTitle);
  cardTitle.className = 'card-title';
  cardTitle.innerText = album.collectionName;
  var cardText = document.createElement('p');
  cardBody.appendChild(cardText);
  cardText.className = 'card-text';
  cardText.innerHTML = album.artistName + '<br/>' + album.trackCount + ' brani<br/>' + new Date(album.releaseDate).getFullYear();

  return cardContainer;
}

function createTracksView(album) {
  var albumView = document.createElement('div');
  albumView.className = 'row';
  albumView.appendChild(createAlbumCard(album[0]));
  albumView.appendChild(createTrackList(album));
  var player = document.createElement('audio');
  player.id = 'player';
  albumView.appendChild(player);
  var navbar = document.getElementById('menu');
  if (navbar.childNodes.length > 2) {
    navbar.removeChild(navbar.lastChild);
  }
  var artist = document.createElement('li');
  artist.className = 'nav-item';
  navbar.appendChild(artist);
  var link = document.createElement('button');
  link.className = 'nav-link btn btn-link';
  artist.appendChild(link);
  link.innerText = album[0].artistName;
  link.onclick = () => {
    albumSearch(album[0].artistId);
  }

  return albumView;
}

function createTableRow(track) {
  var row = document.createElement('tr');
  var cell = document.createElement('th');
  cell.className = 'text-end';
  cell.innerText = track.trackNumber;
  row.appendChild(cell);
  cell = document.createElement('td');
  cell.innerText = track.trackName;
  row.appendChild(cell);
  cell = document.createElement('td');
  cell.className = 'text-center';
  cell.innerText = getTime(track.trackTimeMillis);
  row.appendChild(cell);
  cell = document.createElement('td');
  cell.className = 'text-center';
  cell.innerHTML = '<i id="b' + track.trackNumber + '" class="bi bi-play-btn" onclick="playTrack(\'' + track.previewUrl + '\', ' + track.trackNumber + ')"></i>';
  row.appendChild(cell);
  return row;
}

function createTrackList(album) {
  album = album.splice(1);
  var tracksList = document.createElement('div');
  tracksList.className = 'col-sm-8';
  var table = document.createElement('table');
  tracksList.appendChild(table);
  table.classList = 'table table-striped table-sm border';
  var thead = document.createElement('thead');
  table.appendChild(thead);
  var header = document.createElement('tr');
  header.innerHTML = '<tr><th class="text-center">#</th><th>Titolo</th><th class="text-center">Durata</th><th></th></tr>';
  thead.appendChild(header);
  var tbody = document.createElement('tbody');
  table.appendChild(tbody);
  album.forEach(track => {
    tbody.appendChild(createTableRow(track));
  });
  return tracksList;
}

function getTime(millisec) {
  var seconds = Math.trunc(millisec / 1000);
  var minutes = Math.trunc(seconds / 60);
  seconds = (seconds % 60).toString();
  return minutes + ':' + '0'.repeat(2 - seconds.length) + seconds;
}

function playTrack(url, trackNumber) {
  var player = document.getElementById('player');
  if (!playing) {
    trackPlaying = trackNumber;
    playing = true;
    player.src = url;
    player.play();
    var button = document.getElementById('b' + trackPlaying);
    button.classList = 'bi bi-pause-btn';
  } else {
    player.pause();
    var button = document.getElementById('b' + trackPlaying);
    button.classList = 'bi bi-play-btn';
    if (trackNumber == trackPlaying) {
      playing = false;
      trackPlaying = -1;
    } else {
      trackPlaying = trackNumber;
      player.src = url;
      player.play();
      var button = document.getElementById('b' + trackPlaying);
      button.classList = 'bi bi-pause-btn';
    }
  }

}

initSearchElement();